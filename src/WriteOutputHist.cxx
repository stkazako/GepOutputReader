#include "GepOutputReader/WriteOutputHist.h"

#include "xAODEventInfo/EventInfo.h"

#include "xAODJet/JetContainer.h"
#include "JetEDM/PseudoJetVector.h"
#include "xAODCaloEvent/CaloClusterContainer.h"

#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODCaloEvent/CaloClusterAuxContainer.h"
#include "xAODCaloEvent/CaloCluster.h"

#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertex.h"

#include "CaloEvent/CaloClusterMoment.h"
#include "CaloEvent/CaloClusterMomentStore.h"

using jet::PseudoJetVector;
using xAOD::Jet;

#include <string>

WriteOutputHist::WriteOutputHist(const std::string &name, ISvcLocator *pSvcLocator) : AthAnalysisAlgorithm(name, pSvcLocator)
{

  declareProperty("ClustersList", ClustList);
  declareProperty("JetsList", JetList);
  declareProperty("GetJetConstituentsInfo", m_getJetConstituentsInfo);
  declareProperty("GetJetSeedsInfo", m_getJetSeedsInfo);
}

WriteOutputHist::~WriteOutputHist() {}

StatusCode WriteOutputHist::initialize()
{
  ATH_MSG_INFO("Initializing " << name() << "...");
  //
  //This is called once, before the start of the event loop
  //Retrieves of tools you have configured in the joboptions go here
  //

  // Truth taus
  h_truthTau_et = new TH1D("h_truthTau_pt", "", 60, 2, 120);
  h_truthTau_eta = new TH1D("h_truthTau_eta", "", 100, -4, 4);
  h_truthTau_phi = new TH1D("h_truthTau_phi", "", 100, -3.5, 3.5);

  CHECK(histSvc()->regHist("/STREAM_HIST/h_truthTau_et",  h_truthTau_et));
  CHECK(histSvc()->regHist("/STREAM_HIST/h_truthTau_eta", h_truthTau_eta));
  CHECK(histSvc()->regHist("/STREAM_HIST/h_truthTau_phi", h_truthTau_phi));


  // Clusters
  if (!ClustList.empty()) {
    for (const auto &cl_name : ClustList) {
      std::cout << "Clust name = " << cl_name << std::endl;
      h_cluster_et[cl_name] = new TH1D(("h_" + cl_name + "_et").c_str(), "", 100, 0, 20000);
      h_cluster_eta[cl_name] = new TH1D(("h_" + cl_name + "_eta").c_str(), "", 100, -4, 4);
      h_cluster_phi[cl_name] = new TH1D(("h_" + cl_name + "_phi").c_str(), "", 100, -3.5, 3.5);
      h_cluster_Nclusters[cl_name] = new TH1D(("h_" + cl_name + "_N").c_str(), "", 2500, 0, 2500);

      h_matchedToCluster_truthTauPt[cl_name] = new TH1D(("h_matched" + cl_name + "_et").c_str(), "", 60, 2, 120);
      h_matchedToOneCluster_truthTauPt[cl_name] = new TH1D(("h_matchedTo1" + cl_name + "_et").c_str(), "", 60, 2, 120);

      CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + cl_name + "_et").c_str(), h_cluster_et[cl_name]));
      CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + cl_name + "_eta").c_str(), h_cluster_eta[cl_name]));
      CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + cl_name + "_phi").c_str(), h_cluster_phi[cl_name]));
      CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + cl_name + "_N").c_str(), h_cluster_Nclusters[cl_name]));

      CHECK(histSvc()->regHist(("/STREAM_HIST/h_matched" + cl_name + "_et").c_str(), h_matchedToCluster_truthTauPt[cl_name]));
      CHECK(histSvc()->regHist(("/STREAM_HIST/h_matchedTo1" + cl_name + "_et").c_str(), h_matchedToOneCluster_truthTauPt[cl_name]));

      if (cl_name.find("Calo") != std::string::npos) {
        h_cluster_Ncells[cl_name] = new TH1D(("h_" + cl_name + "_Ncells").c_str(), "", 3000, 0, 3000);
        CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + cl_name + "_Ncells").c_str(), h_cluster_Ncells[cl_name]));
      }
    }
  }


  std :: cout << "m_getJetConstituentsInfo = " << m_getJetConstituentsInfo << std::endl;
  // Jets
  if (!JetList.empty()) {
    for (const auto &jet_name : JetList) {
      h_jet_pt[jet_name] = new TH1D(("h_" + jet_name + "_pt").c_str(), "", 100, 0, 15.e4);
      h_jet_eta[jet_name] = new TH1D(("h_" + jet_name + "_eta").c_str(), "", 100, -4, 4);
      h_jet_phi[jet_name] = new TH1D(("h_" + jet_name + "_phi").c_str(), "", 100, -3.5, 3.5);
      h_jet_m[jet_name] = new TH1D(("h_" + jet_name + "_m").c_str(), "", 100, 0, 15.e4);
      h_jet_Njets[jet_name] = new TH1D(("h_" + jet_name + "_N").c_str(), "", 200, 0, 200);
      h_jet_nConstituents[jet_name] = new TH1D(("h_" + jet_name + "_nConstituents").c_str(), "", 150, 0, 150);

      CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + jet_name + "_pt").c_str(), h_jet_pt[jet_name]));
      CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + jet_name + "_eta").c_str(), h_jet_eta[jet_name]));
      CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + jet_name + "_phi").c_str(), h_jet_phi[jet_name]));
      CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + jet_name + "_m").c_str(), h_jet_m[jet_name]));
      CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + jet_name + "_N").c_str(), h_jet_Njets[jet_name]));
      CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + jet_name + "_nConstituents").c_str(), h_jet_nConstituents[jet_name]));

      if (m_getJetConstituentsInfo) {
	      h_jetConst_et[jet_name] = new TH1D(("h_" + jet_name + "_constituentEt").c_str(), "", 100, 0, 10.e4);
	      h_jetConst_eta[jet_name] = new TH1D(("h_" + jet_name + "_constituentEta").c_str(), "", 100, -4, 4);
	      h_jetConst_phi[jet_name] = new TH1D(("h_" + jet_name + "_constituentPhi").c_str(), "", 100, -3.5, 3.5);

	      CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + jet_name + "_constituentEt").c_str(), h_jetConst_et[jet_name]));
	      CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + jet_name + "_constituentEta").c_str(), h_jetConst_eta[jet_name]));
	      CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + jet_name + "_constituentPhi").c_str(), h_jetConst_phi[jet_name]));
      }
      if (jet_name.find("Cone") != std::string::npos)
      {
        if (m_getJetSeedsInfo)
        {
          h_jetSeed_et[jet_name] = new TH1D(("h_" + jet_name + "_seedEt").c_str(), "", 100, 0, 15.e4);
          h_jetSeed_eta[jet_name] = new TH1D(("h_" + jet_name + "_seedEta").c_str(), "", 100, -4, 4);
          h_jetSeed_phi[jet_name] = new TH1D(("h_" + jet_name + "_seedPhi").c_str(), "", 100, -3.5, 3.5);

          CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + jet_name + "_seedEt").c_str(), h_jetSeed_et[jet_name]));
          CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + jet_name + "_seedEta").c_str(), h_jetSeed_eta[jet_name]));
          CHECK(histSvc()->regHist(("/STREAM_HIST/h_" + jet_name + "_seedPhi").c_str(), h_jetSeed_phi[jet_name]));
        }
      }
    }
  }

  return StatusCode::SUCCESS;
}

StatusCode WriteOutputHist::finalize()
{
  ATH_MSG_INFO("Finalizing " << name() << "...");
  //
  //Things that happen once at the end of the event loop go here
  //

  return StatusCode::SUCCESS;
}

StatusCode WriteOutputHist::execute()
{
  ATH_MSG_DEBUG("Executing " << name() << "...");
  setFilterPassed(false); //optional: start with algorithm not passed

  //--------
  // Clusters

  for (const auto &cl_name : ClustList)
  {
    const xAOD::CaloClusterContainer *clusters = nullptr;
    CHECK(evtStore()->retrieve(clusters, cl_name));
    ATH_MSG_DEBUG("Retrieved " + cl_name);

    h_cluster_Nclusters[cl_name]->Fill(clusters->size());

    for (auto iClust : *clusters)
    {
      h_cluster_et[cl_name]->Fill(iClust->et());
      h_cluster_eta[cl_name]->Fill(iClust->eta());
      h_cluster_phi[cl_name]->Fill(iClust->phi());

      // Ncells
      // custom clusters don't have cells info
      if (cl_name.find("Calo4") != std::string::npos)
      {
        CaloClusterCellLink::const_iterator cellBegin = iClust->cell_begin();
        CaloClusterCellLink::const_iterator cellEnd = iClust->cell_end();
        int nCells = std::distance(cellBegin, cellEnd);
        h_cluster_Ncells[cl_name]->Fill(nCells);
      }
    }
  }

  //----------
  // Jets

  for (const auto &jet_name : JetList)
  {
    const xAOD::JetContainer *jets = nullptr;
    CHECK(evtStore()->retrieve(jets, jet_name));
    ATH_MSG_DEBUG("Retrieved " + jet_name);

    h_jet_Njets[jet_name]->Fill(jets->size());

    for (auto iJet : *jets)
    {
      h_jet_pt[jet_name]->Fill(iJet->pt());
      h_jet_eta[jet_name]->Fill(iJet->eta());
      h_jet_phi[jet_name]->Fill(iJet->phi());
      h_jet_m[jet_name]->Fill(iJet->m());
      h_jet_nConstituents[jet_name]->Fill(iJet->numConstituents());

      if (m_getJetConstituentsInfo)
      {
        int c{0};
        const xAOD::JetConstituentVector constvec = iJet->getConstituents();
        for (xAOD::JetConstituentVector::iterator it = constvec.begin(); it != constvec.end(); it++)
        {
          const xAOD::CaloCluster *cl = static_cast<const xAOD::CaloCluster *>((*it)->rawConstituent());
          h_jetConst_et[jet_name]->Fill(cl->et());
          h_jetConst_eta[jet_name]->Fill(cl->eta());
          h_jetConst_phi[jet_name]->Fill(cl->phi());
          c++;
        }
      }

      if (jet_name.find("Cone") != std::string::npos)
      {
        if (m_getJetSeedsInfo)
        {
          h_jetSeed_et[jet_name]->Fill(iJet->getAttribute<float>("SeedEt"));
          h_jetSeed_eta[jet_name]->Fill(iJet->getAttribute<float>("SeedEta"));
          h_jetSeed_phi[jet_name]->Fill(iJet->getAttribute<float>("SeedPhi"));
          std::cout << "RCut = " << iJet->getAttribute<float>("RCut");
        }
      }
    }
  }

  //******* retriving truth particles
	//TruthParticles (xAOD::TruthParticleAuxContainer_v1) [Truth]
  const xAOD::CaloClusterContainer *clusters = nullptr;

	const xAOD::TruthParticleContainer *truth_particles = nullptr;
	CHECK (evtStore()->retrieve(truth_particles, "TruthParticles"));

	for (auto truth_particle : *truth_particles) {

    //std::cout << "The PDGid is: " << abs(truth_particle->pdgId()) << std::endl;

		if (abs(truth_particle->pdgId()) != 15) continue;

		//note not all truth particles have decay vertices, this statement "truth_particle->decayVtx()" can't
		//be called on all truth particles
		if (truth_particle->hasDecayVtx()){
       //ATH_MSG_INFO ("Truth part has decay vert!");
			 const xAOD::TruthVertex *decayVtx = truth_particle->decayVtx();
			 double finalTauet=0, finalTaueta=0, finalTauphi=0;
			 bool Outgoing_tau = false;

			 float TruthtauPt = truth_particle->pt();
			 //ATH_MSG_INFO("+----truth tau pt" << truth_particle->pt());
			 float TruthtauEta = truth_particle->eta();
			 float TruthtauPhi = truth_particle->phi();
			 TLorentzVector truthTau;
		   truthTau.SetPtEtaPhiM(TruthtauPt, TruthtauEta, TruthtauPhi, 1776);
			 //std::cout <<"The number of outgoing particles: "<<decayVtx->nOutgoingParticles()<<std::endl;

			 TLorentzVector neutrino;
			 const xAOD::TruthParticle *Outparticle;
			 //looping over outgoing particles
			 for (unsigned int v = 0;  v < decayVtx->nOutgoingParticles(); ++v ) {
					Outparticle = decayVtx->outgoingParticle(v);
					//ATH_MSG_INFO("--OutPar id"<< Outparticle->pdgId() << ", and pt "<<Outparticle->pt() <<", status "<< Outparticle->status());
					if (abs(Outparticle->pdgId()) == 15) {
						Outgoing_tau = true;
						finalTauet=Outparticle->pt();
						finalTaueta=Outparticle->eta();
						finalTauphi=Outparticle->phi();
						ATH_MSG_INFO ("outgoing particle has taus !");
					//note, don't use outgoing particles with status =3, they don't add up for pt conservation
					} else if ((Outparticle->isNeutrino()) && (Outparticle->status()!=3) ){

						//std::cout  <<" "<< Outparticle->pt()<<" "<<Outparticle->eta()<< std::endl;
						neutrino.SetPtEtaPhiM(Outparticle->pt(),Outparticle->eta(), Outparticle->phi(), 0);
						//std::cout <<"neutrino " <<neutrino.Et() <<", "<<neutrino.Pt()<<", "<<neutrino.Eta()  <<std::endl;
					}
			 }//end of outgoing particle loop


			//ATH_MSG_INFO ("whether have outgoing taus"<<Outgoing_tau);
			if (Outgoing_tau) continue;
			if (truth_particle->status()==3) continue;
			//std::cout <<"-------truth particle(d) is " <<truth_particle->pdgId()<< "pt "<< truth_particle->pt() <<std::endl;
			//std::cout <<"whether the truth particle has decay vertex: "<<truth_particle->hasDecayVtx() <<", status "<< truth_particle->status() <<std::endl;

      //std::cout << "Tau pt is: " << (truthTau-neutrino).Et() << std::endl;
			//truthTauEt.push_back((truthTau-neutrino).Et());
			//truthTauEta.push_back((truthTau-neutrino).Eta());
			//truthTauPhi.push_back((truthTau-neutrino).Phi());

      h_truthTau_et->Fill((truthTau-neutrino).Et()*1e-3);
      h_truthTau_eta->Fill((truthTau-neutrino).Eta());
      h_truthTau_phi->Fill((truthTau-neutrino).Phi());

      for (const auto &cl_name : ClustList){

        CHECK(evtStore()->retrieve(clusters, cl_name));
        ATH_MSG_DEBUG("Retrieved " + cl_name);
        //cl_name: Calo422Cut0GeVTopoClusters, Calo422SKCut0GeVTopoClusters, Calo420Cut0GeVTopoClusters, Calo420SKCut0GeVTopoClusters_et

        double deltaR=0;
        for (auto iClust : *clusters){
          deltaR = TMath::Sqrt((((truthTau-neutrino).Eta()-iClust->eta())*((truthTau-neutrino).Eta()-iClust->eta()))+(((truthTau-neutrino).Phi()-iClust->phi())*((truthTau-neutrino).Phi()-iClust->phi())));
          if (deltaR < 0.1){
            //std::cout << deltaR << std::endl;
            h_matchedToCluster_truthTauPt[cl_name]->Fill((truthTau-neutrino).Et()*1e-3);
          }
		    }//if truth  has vertex

        for (auto iClust : *clusters){
          deltaR = TMath::Sqrt((((truthTau-neutrino).Eta()-iClust->eta())*((truthTau-neutrino).Eta()-iClust->eta()))+(((truthTau-neutrino).Phi()-iClust->phi())*((truthTau-neutrino).Phi()-iClust->phi())));
          if (deltaR < 0.1){
            //std::cout << deltaR << std::endl;
            h_matchedToOneCluster_truthTauPt[cl_name]->Fill((truthTau-neutrino).Et()*1e-3);
            break;
          }
		    }//if truth  has vertex
      }

			Outgoing_tau = false;
    }
	}//end of truth particle loop

  //-------

  setFilterPassed(true);
  return StatusCode::SUCCESS;
}
