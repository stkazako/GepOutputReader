# GepOutputReader

Package to read output of TrigL0GepPerf and produce histograms and ntuples.

# The output files can be found here:

/afs/cern.ch/work/s/stkazako/public/TriggerOutputs
